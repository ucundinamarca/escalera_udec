/*jslint */
/*global AdobeEdge: false, window: false, document: false, console:false, alert: false */
(function (compId) {

    "use strict";
    var im='images/',
        aud='media/',
        vid='media/',
        js='js/',
        fonts = {
        },
        opts = {
            'gAudioPreloadPreference': 'auto',
            'gVideoPreloadPreference': 'auto'
        },
        resources = [
        ],
        scripts = [
        ],
        symbols = {
            "stage": {
                version: "6.0.0",
                minimumCompatibleVersion: "5.0.0",
                build: "6.0.0.400",
                scaleToFit: "both",
                centerStage: "both",
                resizeInstances: false,
                content: {
                    dom: [
                        {
                            id: 'Group',
                            type: 'group',
                            rect: ['0px', '0', '1024', '640', 'auto', 'auto'],
                            c: [
                            {
                                id: 'background',
                                type: 'image',
                                rect: ['-4px', '1px', '1024px', '640px', 'auto', 'auto'],
                                opacity: '1',
                                fill: ["rgba(0,0,0,0)",im+"background.png",'0px','0px']
                            },
                            {
                                id: 'comienzo',
                                type: 'image',
                                rect: ['196px', '479px', '105px', '127px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"comienzo.png",'0px','0px']
                            },
                            {
                                id: 'final',
                                type: 'image',
                                rect: ['291px', '19px', '105px', '129px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"final.png",'0px','0px']
                            },
                            {
                                id: 'Recurso_2',
                                type: 'image',
                                rect: ['2px', '30px', '293px', '127px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Recurso_2.png",'0px','0px']
                            },
                            {
                                id: 'Recurso_22',
                                type: 'image',
                                rect: ['323px', '229px', '116px', '209px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Recurso_22.png",'0px','0px']
                            },
                            {
                                id: 'Recurso_21',
                                type: 'image',
                                rect: ['760px', '362px', '110px', '211px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Recurso_21.png",'0px','0px']
                            },
                            {
                                id: 'Text3',
                                type: 'text',
                                rect: ['26px', '43px', '105px', '27px', 'auto', 'auto'],
                                text: "<p style=\"margin: 0px;\">​Pregunta</p>",
                                userClass: "game",
                                font: ['Arial, Helvetica, sans-serif', [20, "px"], "rgba(0,0,0,1)", "normal", "none", "", "break-word", "normal"]
                            },
                            {
                                id: 'Text3Copy2',
                                type: 'text',
                                rect: ['171px', '110px', '105px', '27px', 'auto', 'auto'],
                                text: "<p style=\"margin: 0px;\">​minuto(s)</p>",
                                userClass: "game",
                                font: ['Arial, Helvetica, sans-serif', [20, "px"], "rgba(0,0,0,1)", "normal", "none", "", "break-word", "normal"]
                            },
                            {
                                id: 'time',
                                type: 'text',
                                rect: ['162px', '75px', '106px', '33px', 'auto', 'auto'],
                                text: "<p style=\"margin: 0px;\">​10:00</p>",
                                align: "center",
                                font: ['Arial, Helvetica, sans-serif', [34, "px"], "rgba(241,22,35,1.00)", "400", "none", "italic", "break-word", "normal"],
                                textStyle: ["", "", "", "", "none"]
                            },
                            {
                                id: 'n2',
                                type: 'text',
                                rect: ['72px', '76px', '53px', '33px', 'auto', 'auto'],
                                text: "<p style=\"margin: 0px;\">​40</p>",
                                align: "center",
                                font: ['Arial, Helvetica, sans-serif', [34, "px"], "rgba(241,22,35,1.00)", "400", "none", "italic", "break-word", "normal"],
                                textStyle: ["", "", "", "", "none"]
                            },
                            {
                                id: 'n1',
                                type: 'text',
                                rect: ['10px', '76px', '53px', '33px', 'auto', 'auto'],
                                text: "<p style=\"margin: 0px;\">​0</p>",
                                align: "center",
                                font: ['Arial, Helvetica, sans-serif', [34, "px"], "rgba(241,22,35,1.00)", "400", "none", "italic", "break-word", "normal"],
                                textStyle: ["", "", "", "", "none"]
                            },
                            {
                                id: 'Text3Copy',
                                type: 'text',
                                rect: ['179px', '43px', '105px', '27px', 'auto', 'auto'],
                                text: "<p style=\"margin: 0px;\">​Tiempo</p>",
                                userClass: "game",
                                font: ['Arial, Helvetica, sans-serif', [20, "px"], "rgba(0,0,0,1)", "normal", "none", "", "break-word", "normal"]
                            },
                            {
                                id: 'Text3Copy3',
                                type: 'text',
                                rect: ['54px', '76px', '28px', '20px', 'auto', 'auto'],
                                text: "<p style=\"margin: 0px;\">​de</p>",
                                align: "center",
                                userClass: "game",
                                font: ['Arial, Helvetica, sans-serif', [15, "px"], "rgba(0,0,0,1)", "normal", "none", "", "break-word", "normal"]
                            }]
                        },
                        {
                            id: 'ava',
                            type: 'group',
                            rect: ['223', '443', '53', '116', 'auto', 'auto'],
                            c: [
                            {
                                id: 'avatar',
                                symbolName: 'avatar',
                                type: 'rect',
                                rect: ['0px', '0px', '53', '116', 'auto', 'auto']
                            }]
                        },
                        {
                            id: 'stage_dado',
                            type: 'group',
                            rect: ['-9px', '1px', '1038', '640', 'auto', 'auto'],
                            c: [
                            {
                                id: 'Rectangle',
                                type: 'rect',
                                rect: ['0px', '0px', '1038px', '640px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0.64)"],
                                stroke: [0,"rgba(0,0,0,1)","none"]
                            },
                            {
                                id: 's_dado',
                                type: 'group',
                                rect: ['250', '175', '542', '316', 'auto', 'auto'],
                                c: [
                                {
                                    id: 'fondo',
                                    type: 'image',
                                    rect: ['0px', '0px', '542px', '316px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"fondo.png",'0px','0px']
                                },
                                {
                                    id: 'btn_seg',
                                    type: 'group',
                                    rect: ['205', '235', '149', '51px', 'auto', 'auto'],
                                    cursor: 'pointer',
                                    c: [
                                    {
                                        id: 'btn_seg_',
                                        type: 'image',
                                        rect: ['0px', '0px', '149px', '51px', 'auto', 'auto'],
                                        cursor: 'pointer',
                                        fill: ["rgba(0,0,0,0)",im+"btn_seg.png",'0px','0px']
                                    },
                                    {
                                        id: 'Rectangle4',
                                        type: 'rect',
                                        rect: ['27px', '5px', '98px', '40px', 'auto', 'auto'],
                                        fill: ["rgba(207,208,36,1.00)"],
                                        stroke: [0,"rgb(0, 0, 0)","none"]
                                    },
                                    {
                                        id: 'Text4',
                                        type: 'text',
                                        rect: ['5px', '19px', '139px', '30px', 'auto', 'auto'],
                                        text: "<p style=\"margin: 0px; text-align: center;\">​<span style=\"color: rgb(255, 255, 255);\">Siguiente</span></p>",
                                        align: "left",
                                        font: ['Arial, Helvetica, sans-serif', [20, "px"], "rgba(0,0,0,1)", "400", "none", "normal", "break-word", "normal"],
                                        textStyle: ["", "", "", "", "none"]
                                    }]
                                },
                                {
                                    id: 'dado',
                                    symbolName: 'dado',
                                    type: 'rect',
                                    rect: ['242px', '130px', '83', '91', 'auto', 'auto'],
                                    cursor: 'pointer'
                                },
                                {
                                    id: 'd_1',
                                    type: 'image',
                                    rect: ['242px', '130px', '83px', '90px', 'auto', 'auto'],
                                    cursor: 'pointer',
                                    fill: ["rgba(0,0,0,0)",im+"dado_1.png",'0px','0px'],
                                    userClass: "dados"
                                },
                                {
                                    id: 'd_2',
                                    type: 'image',
                                    rect: ['242px', '130px', '83px', '91px', 'auto', 'auto'],
                                    cursor: 'pointer',
                                    fill: ["rgba(0,0,0,0)",im+"d_2.png",'0px','0px'],
                                    userClass: "dados"
                                },
                                {
                                    id: 'd_3',
                                    type: 'image',
                                    rect: ['242px', '130px', '83px', '91px', 'auto', 'auto'],
                                    cursor: 'pointer',
                                    fill: ["rgba(0,0,0,0)",im+"d_3.png",'0px','0px'],
                                    userClass: "dados"
                                },
                                {
                                    id: 'd_4',
                                    type: 'image',
                                    rect: ['242px', '130px', '83px', '91px', 'auto', 'auto'],
                                    cursor: 'pointer',
                                    fill: ["rgba(0,0,0,0)",im+"d_4.png",'0px','0px'],
                                    userClass: "dados"
                                },
                                {
                                    id: 'd_5',
                                    type: 'image',
                                    rect: ['242px', '130px', '83px', '91px', 'auto', 'auto'],
                                    cursor: 'pointer',
                                    fill: ["rgba(0,0,0,0)",im+"d_5.png",'0px','0px'],
                                    userClass: "dados"
                                },
                                {
                                    id: 'd_6',
                                    type: 'image',
                                    rect: ['242px', '130px', '83px', '91px', 'auto', 'auto'],
                                    cursor: 'pointer',
                                    fill: ["rgba(0,0,0,0)",im+"d_6.png",'0px','0px'],
                                    userClass: "dados"
                                },
                                {
                                    id: 'capa',
                                    type: 'rect',
                                    rect: ['210px', '114px', '140px', '113px', 'auto', 'auto'],
                                    cursor: 'no-drop',
                                    fill: ["rgba(0,0,0,0.00)"],
                                    stroke: [0,"rgb(0, 0, 0)","none"]
                                },
                                {
                                    id: 'Text',
                                    type: 'text',
                                    rect: ['30px', '45px', '475px', '71px', 'auto', 'auto'],
                                    text: "<p style=\"margin: 0px; text-align: center;\">​Por favor de clic en el dado para hacerlo girar y saber cuántos pasos debe avanzar el funcionario.</p>",
                                    userClass: "game",
                                    font: ['Arial, Helvetica, sans-serif', [17, "px"], "rgba(0,0,0,1)", "normal", "none", "", "break-word", "normal"],
                                    textStyle: ["", "", "22px", "", ""]
                                }]
                            }]
                        },
                        {
                            id: 'stage_instruccion',
                            type: 'group',
                            rect: ['-9px', '1px', '1038', '640', 'auto', 'auto'],
                            c: [
                            {
                                id: 'Rectangle2',
                                type: 'rect',
                                rect: ['0px', '0px', '1038px', '640px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0.64)"],
                                stroke: [0,"rgba(0,0,0,1)","none"]
                            },
                            {
                                id: 'contenido',
                                type: 'group',
                                rect: ['250', '175', '542', '316', 'auto', 'auto'],
                                c: [
                                {
                                    id: 'fondo2',
                                    type: 'image',
                                    rect: ['0px', '0px', '542px', '316px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"fondo.png",'0px','0px']
                                },
                                {
                                    id: 'btn_seg2',
                                    type: 'group',
                                    rect: ['205', '235', '149', '51', 'auto', 'auto'],
                                    cursor: 'pointer',
                                    c: [
                                    {
                                        id: 'btn_seg2_',
                                        type: 'image',
                                        rect: ['0px', '0px', '149px', '51px', 'auto', 'auto'],
                                        cursor: 'pointer',
                                        fill: ["rgba(0,0,0,0)",im+"btn_seg.png",'0px','0px']
                                    },
                                    {
                                        id: 'Rectangle4Copy',
                                        type: 'rect',
                                        rect: ['27px', '5px', '98px', '40px', 'auto', 'auto'],
                                        fill: ["rgba(207,208,36,1.00)"],
                                        stroke: [0,"rgb(0, 0, 0)","none"]
                                    },
                                    {
                                        id: 'Text4Copy',
                                        type: 'text',
                                        rect: ['5px', '19px', '139px', '30px', 'auto', 'auto'],
                                        text: "<p style=\"margin: 0px; text-align: center;\">​<span style=\"color: rgb(255, 255, 255);\">Siguiente</span></p>",
                                        align: "left",
                                        font: ['Arial, Helvetica, sans-serif', [20, "px"], "rgba(0,0,0,1)", "400", "none", "normal", "break-word", "normal"],
                                        textStyle: ["", "", "", "", "none"]
                                    }]
                                },
                                {
                                    id: 'icon1',
                                    type: 'ellipse',
                                    rect: ['262px', '212px', '12px', '13px', 'auto', 'auto'],
                                    cursor: 'pointer',
                                    borderRadius: ["50%", "50%", "50%", "50%"],
                                    fill: ["rgba(192,155,155,1.00)"],
                                    stroke: [0,"rgb(0, 0, 0)","none"]
                                },
                                {
                                    id: 'icon2',
                                    type: 'ellipse',
                                    rect: ['287px', '212px', '12px', '13px', 'auto', 'auto'],
                                    cursor: 'pointer',
                                    borderRadius: ["50%", "50%", "50%", "50%"],
                                    fill: ["rgba(192,155,155,1.00)"],
                                    stroke: [0,"rgb(0, 0, 0)","none"]
                                },
                                {
                                    id: 'page_2',
                                    type: 'group',
                                    rect: ['26px', '28', '495', '153', 'auto', 'auto'],
                                    userClass: "ins",
                                    c: [
                                    {
                                        id: 'TextCopy2',
                                        type: 'text',
                                        rect: ['3px', '58px', '492px', '95px', 'auto', 'auto'],
                                        text: "<p style=\"margin: 0px; text-align: center;\">​Recuerda jugar la escalera con COMPROMISO, DILIGENCIA Y TRANSPARENCIA. Lanza los dados, y responde las preguntas que se desplegarán a tu paso.</p>",
                                        userClass: "game",
                                        font: ['Arial, Helvetica, sans-serif', [13, "px"], "rgba(0,0,0,1)", "normal", "none", "", "break-word", "normal"]
                                    },
                                    {
                                        id: 'TextCopy3',
                                        type: 'text',
                                        rect: ['0px', '0px', '492px', '31px', 'auto', 'auto'],
                                        text: "<p style=\"margin: 0px; text-align: center;\">​Instrucción</p>",
                                        userClass: "game",
                                        font: ['Arial, Helvetica, sans-serif', [22, "px"], "rgba(0,0,0,1)", "normal", "none", "", "break-word", "normal"]
                                    }]
                                },
                                {
                                    id: 'page_1',
                                    type: 'group',
                                    rect: ['26px', '28', '495', '153', 'auto', 'auto'],
                                    userClass: "ins",
                                    c: [
                                    {
                                        id: 'TextCopy5',
                                        type: 'text',
                                        rect: ['3px', '-10px', '492px', '95px', 'auto', 'auto'],
                                        text: "<p style=\"margin: 0px;\">​<span style=\"font-size: 15px;\">En el contexto de este juego, el participante podrá interiorizar la cultura de la legalidad a partir del Código de Integridad del Servicio Público; y así mismo, identificar y declarar el conflicto de intereses como un mecanismo preventivo en la lucha contra la corrupción; para que, finalmente, la transparencia en gestión se convierta en una virtud en el servicio. Igualmente, trata la identificación y cumplimiento de las normas desde una forma reflexiva que pretende activar en el servidor público la importancia de emprender un cambio cultural que rompa con el refrán popular “hecha la ley, hecha la trampa”, basados en la ineludible tarea de cualquier servidor público: cumplir y hacer cumplir las normas.</span></p>",
                                        align: "justify",
                                        userClass: "game",
                                        font: ['Arial, Helvetica, sans-serif', [13, "px"], "rgba(0,0,0,1)", "normal", "none", "", "break-word", "normal"]
                                    }]
                                }]
                            }]
                        },
                        {
                            id: 'stage_question',
                            type: 'group',
                            rect: ['-9', '1', '1038', '640', 'auto', 'auto'],
                            c: [
                            {
                                id: 'RectangleCopy',
                                type: 'rect',
                                rect: ['0px', '0px', '1038px', '640px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0.64)"],
                                stroke: [0,"rgba(0,0,0,1)","none"]
                            },
                            {
                                id: 's_q',
                                type: 'group',
                                rect: ['250', '116', '542', '406', 'auto', 'auto'],
                                c: [
                                {
                                    id: 'box2',
                                    type: 'image',
                                    rect: ['0px', '0px', '542px', '406px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"box2.png",'0px','0px']
                                },
                                {
                                    id: 'Text2',
                                    type: 'text',
                                    rect: ['34px', '63px', '471px', '305px', 'auto', 'auto'],
                                    text: "<p style=\"margin: 0px;\">​</p>",
                                    align: "left",
                                    userClass: "game",
                                    font: ['Arial, Helvetica, sans-serif', [15, "px"], "rgba(0,0,0,1)", "400", "none", "normal", "break-word", "normal"],
                                    textStyle: ["", "", "", "", "none"]
                                },
                                {
                                    id: 'TextCopy',
                                    type: 'text',
                                    rect: ['30px', '13px', '475px', '27px', 'auto', 'auto'],
                                    text: "<p style=\"margin: 0px; text-align: center;\">Pregunta</p>",
                                    userClass: "game",
                                    font: ['Arial, Helvetica, sans-serif', [24, ""], "rgba(0,0,0,1)", "normal", "none", "", "break-word", "normal"]
                                }]
                            }]
                        },
                        {
                            id: 'preload',
                            type: 'group',
                            rect: ['0', '0', '1024', '640', 'auto', 'auto'],
                            c: [
                            {
                                id: 'Rectangle3',
                                type: 'rect',
                                rect: ['0px', '0px', '1024px', '640px', 'auto', 'auto'],
                                fill: ["rgba(255,255,255,1)"],
                                stroke: [0,"rgb(0, 0, 0)","none"]
                            },
                            {
                                id: 'logo',
                                type: 'image',
                                rect: ['462px', '272px', '100px', '100px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"logo.svg",'0px','0px']
                            }]
                        }
                    ],
                    style: {
                        '${Stage}': {
                            isStage: true,
                            rect: ['null', 'null', '1024px', '640px', 'auto', 'auto'],
                            overflow: 'hidden',
                            fill: ["rgba(255,255,255,1)"]
                        }
                    }
                },
                timeline: {
                    duration: 0,
                    autoPlay: true,
                    data: [
                        [
                            "eid931",
                            "left",
                            0,
                            0,
                            "linear",
                            "${avatar}",
                            '0px',
                            '0px'
                        ],
                        [
                            "eid4162",
                            "left",
                            0,
                            0,
                            "linear",
                            "${dado}",
                            '242px',
                            '242px'
                        ],
                        [
                            "eid4161",
                            "top",
                            0,
                            0,
                            "linear",
                            "${dado}",
                            '130px',
                            '130px'
                        ],
                        [
                            "eid932",
                            "top",
                            0,
                            0,
                            "linear",
                            "${avatar}",
                            '0px',
                            '0px'
                        ],
                            [ "eid2128", "trigger", 0, function executeSymbolFunction(e, data) { this._executeSymbolAction(e, data); }, ['stop', '${dado}', [] ] ],
                            [ "eid4208", "trigger", 0, function executeSymbolFunction(e, data) { this._executeSymbolAction(e, data); }, ['stop', '${avatar}', [] ] ]
                    ]
                }
            },
            "avatar": {
                version: "6.0.0",
                minimumCompatibleVersion: "5.0.0",
                build: "6.0.0.400",
                scaleToFit: "none",
                centerStage: "none",
                resizeInstances: false,
                content: {
                    dom: [
                        {
                            rect: ['19px', '0px', '24px', '34px', 'auto', 'auto'],
                            id: 'Recurso_15',
                            transform: [[], ['12']],
                            type: 'image',
                            fill: ['rgba(0,0,0,0)', 'images/Recurso_15.png', '0px', '0px']
                        },
                        {
                            id: 'pie2',
                            type: 'image',
                            rect: ['1px', '63px', '30px', '53px', 'auto', 'auto'],
                            fill: ['rgba(0,0,0,0)', 'images/Recurso_18.png', '0px', '0px']
                        },
                        {
                            rect: ['19px', '60px', '34px', '56px', 'auto', 'auto'],
                            id: 'pie1',
                            transform: [[], ['40']],
                            type: 'image',
                            fill: ['rgba(0,0,0,0)', 'images/Recurso_17.png', '0px', '0px']
                        },
                        {
                            id: 'Recurso_16',
                            type: 'image',
                            rect: ['25px', '37px', '25px', '30px', 'auto', 'auto'],
                            fill: ['rgba(0,0,0,0)', 'images/Recurso_16.png', '0px', '0px']
                        },
                        {
                            id: 'Recurso_19',
                            type: 'image',
                            rect: ['14px', '26px', '22px', '44px', 'auto', 'auto'],
                            fill: ['rgba(0,0,0,0)', 'images/Recurso_19.png', '0px', '0px']
                        },
                        {
                            rect: ['0px', '35px', '27px', '57px', 'auto', 'auto'],
                            id: 'Recurso_14',
                            transform: [[], ['-21']],
                            type: 'image',
                            fill: ['rgba(0,0,0,0)', 'images/Recurso_14.png', '0px', '0px']
                        }
                    ],
                    style: {
                        '${symbolSelector}': {
                            isStage: 'true',
                            rect: [undefined, undefined, '53px', '116px']
                        }
                    }
                },
                timeline: {
                    duration: 500,
                    autoPlay: true,
                    data: [
                        [
                            "eid51",
                            "rotateZ",
                            0,
                            250,
                            "linear",
                            "${Recurso_14}",
                            '0deg',
                            '-21deg'
                        ],
                        [
                            "eid54",
                            "rotateZ",
                            250,
                            250,
                            "linear",
                            "${Recurso_14}",
                            '-21deg',
                            '0deg'
                        ],
                        [
                            "eid6",
                            "-webkit-transform-origin",
                            0,
                            0,
                            "linear",
                            "${pie1}",
                            [28,15],
                            [28,15],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid6597",
                            "-moz-transform-origin",
                            0,
                            0,
                            "linear",
                            "${pie1}",
                            [28,15],
                            [28,15],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid6598",
                            "-ms-transform-origin",
                            0,
                            0,
                            "linear",
                            "${pie1}",
                            [28,15],
                            [28,15],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid6599",
                            "msTransformOrigin",
                            0,
                            0,
                            "linear",
                            "${pie1}",
                            [28,15],
                            [28,15],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid6600",
                            "-o-transform-origin",
                            0,
                            0,
                            "linear",
                            "${pie1}",
                            [28,15],
                            [28,15],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid6601",
                            "transform-origin",
                            0,
                            0,
                            "linear",
                            "${pie1}",
                            [28,15],
                            [28,15],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid13",
                            "-webkit-transform-origin",
                            276,
                            0,
                            "linear",
                            "${pie1}",
                            [28,15],
                            [28,15],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid6602",
                            "-moz-transform-origin",
                            276,
                            0,
                            "linear",
                            "${pie1}",
                            [28,15],
                            [28,15],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid6603",
                            "-ms-transform-origin",
                            276,
                            0,
                            "linear",
                            "${pie1}",
                            [28,15],
                            [28,15],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid6604",
                            "msTransformOrigin",
                            276,
                            0,
                            "linear",
                            "${pie1}",
                            [28,15],
                            [28,15],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid6605",
                            "-o-transform-origin",
                            276,
                            0,
                            "linear",
                            "${pie1}",
                            [28,15],
                            [28,15],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid6606",
                            "transform-origin",
                            276,
                            0,
                            "linear",
                            "${pie1}",
                            [28,15],
                            [28,15],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid171",
                            "rotateZ",
                            0,
                            250,
                            "linear",
                            "${Recurso_15}",
                            '12deg',
                            '0deg'
                        ],
                        [
                            "eid179",
                            "rotateZ",
                            250,
                            250,
                            "linear",
                            "${Recurso_15}",
                            '0deg',
                            '12deg'
                        ],
                        [
                            "eid167",
                            "-webkit-transform-origin",
                            0,
                            0,
                            "linear",
                            "${Recurso_15}",
                            [40,85],
                            [40,85],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid6607",
                            "-moz-transform-origin",
                            0,
                            0,
                            "linear",
                            "${Recurso_15}",
                            [40,85],
                            [40,85],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid6608",
                            "-ms-transform-origin",
                            0,
                            0,
                            "linear",
                            "${Recurso_15}",
                            [40,85],
                            [40,85],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid6609",
                            "msTransformOrigin",
                            0,
                            0,
                            "linear",
                            "${Recurso_15}",
                            [40,85],
                            [40,85],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid6610",
                            "-o-transform-origin",
                            0,
                            0,
                            "linear",
                            "${Recurso_15}",
                            [40,85],
                            [40,85],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid6611",
                            "transform-origin",
                            0,
                            0,
                            "linear",
                            "${Recurso_15}",
                            [40,85],
                            [40,85],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid176",
                            "-webkit-transform-origin",
                            250,
                            0,
                            "linear",
                            "${Recurso_15}",
                            [40,85],
                            [40,85],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid6612",
                            "-moz-transform-origin",
                            250,
                            0,
                            "linear",
                            "${Recurso_15}",
                            [40,85],
                            [40,85],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid6613",
                            "-ms-transform-origin",
                            250,
                            0,
                            "linear",
                            "${Recurso_15}",
                            [40,85],
                            [40,85],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid6614",
                            "msTransformOrigin",
                            250,
                            0,
                            "linear",
                            "${Recurso_15}",
                            [40,85],
                            [40,85],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid6615",
                            "-o-transform-origin",
                            250,
                            0,
                            "linear",
                            "${Recurso_15}",
                            [40,85],
                            [40,85],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid6616",
                            "transform-origin",
                            250,
                            0,
                            "linear",
                            "${Recurso_15}",
                            [40,85],
                            [40,85],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid12",
                            "rotateZ",
                            37,
                            190,
                            "linear",
                            "${pie2}",
                            '0deg',
                            '-36deg'
                        ],
                        [
                            "eid17",
                            "rotateZ",
                            311,
                            189,
                            "linear",
                            "${pie2}",
                            '-36deg',
                            '0deg'
                        ],
                        [
                            "eid9",
                            "rotateZ",
                            0,
                            125,
                            "linear",
                            "${pie1}",
                            '0deg',
                            '40deg'
                        ],
                        [
                            "eid15",
                            "rotateZ",
                            276,
                            146,
                            "linear",
                            "${pie1}",
                            '40deg',
                            '0deg'
                        ],
                        [
                            "eid58",
                            "rotateZ",
                            0,
                            215,
                            "linear",
                            "${Recurso_16}",
                            '0deg',
                            '21deg'
                        ],
                        [
                            "eid61",
                            "rotateZ",
                            215,
                            285,
                            "linear",
                            "${Recurso_16}",
                            '21deg',
                            '0deg'
                        ],
                        [
                            "eid5",
                            "-webkit-transform-origin",
                            0,
                            0,
                            "linear",
                            "${pie2}",
                            [67,15],
                            [67,15],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid6617",
                            "-moz-transform-origin",
                            0,
                            0,
                            "linear",
                            "${pie2}",
                            [67,15],
                            [67,15],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid6618",
                            "-ms-transform-origin",
                            0,
                            0,
                            "linear",
                            "${pie2}",
                            [67,15],
                            [67,15],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid6619",
                            "msTransformOrigin",
                            0,
                            0,
                            "linear",
                            "${pie2}",
                            [67,15],
                            [67,15],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid6620",
                            "-o-transform-origin",
                            0,
                            0,
                            "linear",
                            "${pie2}",
                            [67,15],
                            [67,15],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid6621",
                            "transform-origin",
                            0,
                            0,
                            "linear",
                            "${pie2}",
                            [67,15],
                            [67,15],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid55",
                            "-webkit-transform-origin",
                            0,
                            0,
                            "linear",
                            "${Recurso_16}",
                            [14,26],
                            [14,26],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid6622",
                            "-moz-transform-origin",
                            0,
                            0,
                            "linear",
                            "${Recurso_16}",
                            [14,26],
                            [14,26],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid6623",
                            "-ms-transform-origin",
                            0,
                            0,
                            "linear",
                            "${Recurso_16}",
                            [14,26],
                            [14,26],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid6624",
                            "msTransformOrigin",
                            0,
                            0,
                            "linear",
                            "${Recurso_16}",
                            [14,26],
                            [14,26],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid6625",
                            "-o-transform-origin",
                            0,
                            0,
                            "linear",
                            "${Recurso_16}",
                            [14,26],
                            [14,26],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid6626",
                            "transform-origin",
                            0,
                            0,
                            "linear",
                            "${Recurso_16}",
                            [14,26],
                            [14,26],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid59",
                            "-webkit-transform-origin",
                            215,
                            0,
                            "linear",
                            "${Recurso_16}",
                            [14,26],
                            [14,26],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid6627",
                            "-moz-transform-origin",
                            215,
                            0,
                            "linear",
                            "${Recurso_16}",
                            [14,26],
                            [14,26],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid6628",
                            "-ms-transform-origin",
                            215,
                            0,
                            "linear",
                            "${Recurso_16}",
                            [14,26],
                            [14,26],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid6629",
                            "msTransformOrigin",
                            215,
                            0,
                            "linear",
                            "${Recurso_16}",
                            [14,26],
                            [14,26],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid6630",
                            "-o-transform-origin",
                            215,
                            0,
                            "linear",
                            "${Recurso_16}",
                            [14,26],
                            [14,26],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid6631",
                            "transform-origin",
                            215,
                            0,
                            "linear",
                            "${Recurso_16}",
                            [14,26],
                            [14,26],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid48",
                            "-webkit-transform-origin",
                            0,
                            0,
                            "linear",
                            "${Recurso_14}",
                            [81,7],
                            [81,7],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid6632",
                            "-moz-transform-origin",
                            0,
                            0,
                            "linear",
                            "${Recurso_14}",
                            [81,7],
                            [81,7],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid6633",
                            "-ms-transform-origin",
                            0,
                            0,
                            "linear",
                            "${Recurso_14}",
                            [81,7],
                            [81,7],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid6634",
                            "msTransformOrigin",
                            0,
                            0,
                            "linear",
                            "${Recurso_14}",
                            [81,7],
                            [81,7],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid6635",
                            "-o-transform-origin",
                            0,
                            0,
                            "linear",
                            "${Recurso_14}",
                            [81,7],
                            [81,7],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid6636",
                            "transform-origin",
                            0,
                            0,
                            "linear",
                            "${Recurso_14}",
                            [81,7],
                            [81,7],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid52",
                            "-webkit-transform-origin",
                            250,
                            0,
                            "linear",
                            "${Recurso_14}",
                            [81,7],
                            [81,7],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid6637",
                            "-moz-transform-origin",
                            250,
                            0,
                            "linear",
                            "${Recurso_14}",
                            [81,7],
                            [81,7],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid6638",
                            "-ms-transform-origin",
                            250,
                            0,
                            "linear",
                            "${Recurso_14}",
                            [81,7],
                            [81,7],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid6639",
                            "msTransformOrigin",
                            250,
                            0,
                            "linear",
                            "${Recurso_14}",
                            [81,7],
                            [81,7],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid6640",
                            "-o-transform-origin",
                            250,
                            0,
                            "linear",
                            "${Recurso_14}",
                            [81,7],
                            [81,7],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid6641",
                            "transform-origin",
                            250,
                            0,
                            "linear",
                            "${Recurso_14}",
                            [81,7],
                            [81,7],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ]
                    ]
                }
            },
            "dado": {
                version: "6.0.0",
                minimumCompatibleVersion: "5.0.0",
                build: "6.0.0.400",
                scaleToFit: "none",
                centerStage: "none",
                resizeInstances: false,
                content: {
                    dom: [
                        {
                            type: 'image',
                            display: 'block',
                            rect: ['0px', '0px', '83px', '91px', 'auto', 'auto'],
                            id: 'd_2',
                            fill: ['rgba(0,0,0,0)', 'images/d_2.png', '0px', '0px']
                        },
                        {
                            type: 'image',
                            display: 'none',
                            rect: ['0px', '0px', '83px', '91px', 'auto', 'auto'],
                            id: 'd_4',
                            fill: ['rgba(0,0,0,0)', 'images/d_4.png', '0px', '0px']
                        },
                        {
                            type: 'image',
                            display: 'none',
                            rect: ['0px', '0px', '83px', '91px', 'auto', 'auto'],
                            id: 'd_6',
                            fill: ['rgba(0,0,0,0)', 'images/d_6.png', '0px', '0px']
                        },
                        {
                            type: 'image',
                            display: 'none',
                            rect: ['0px', '0px', '83px', '91px', 'auto', 'auto'],
                            id: 'd_3',
                            fill: ['rgba(0,0,0,0)', 'images/d_3.png', '0px', '0px']
                        },
                        {
                            type: 'image',
                            display: 'none',
                            rect: ['0px', '0px', '83px', '91px', 'auto', 'auto'],
                            id: 'd_5',
                            fill: ['rgba(0,0,0,0)', 'images/d_5.png', '0px', '0px']
                        },
                        {
                            type: 'image',
                            display: 'none',
                            rect: ['0px', '0px', '83px', '90px', 'auto', 'auto'],
                            id: 'd_1',
                            fill: ['rgba(0,0,0,0)', 'images/dado_1.png', '0px', '0px']
                        }
                    ],
                    style: {
                        '${symbolSelector}': {
                            rect: [null, null, '83px', '91px']
                        }
                    }
                },
                timeline: {
                    duration: 1250,
                    autoPlay: true,
                    data: [
                        [
                            "eid1207",
                            "display",
                            0,
                            0,
                            "linear",
                            "${d_6}",
                            'none',
                            'none'
                        ],
                        [
                            "eid1209",
                            "display",
                            250,
                            0,
                            "linear",
                            "${d_6}",
                            'none',
                            'block'
                        ],
                        [
                            "eid1801",
                            "display",
                            630,
                            0,
                            "linear",
                            "${d_6}",
                            'block',
                            'none'
                        ],
                        [
                            "eid1802",
                            "display",
                            880,
                            0,
                            "linear",
                            "${d_6}",
                            'none',
                            'block'
                        ],
                        [
                            "eid1205",
                            "display",
                            0,
                            0,
                            "linear",
                            "${d_4}",
                            'none',
                            'none'
                        ],
                        [
                            "eid1208",
                            "display",
                            120,
                            0,
                            "linear",
                            "${d_4}",
                            'none',
                            'block'
                        ],
                        [
                            "eid1804",
                            "display",
                            630,
                            0,
                            "linear",
                            "${d_4}",
                            'block',
                            'none'
                        ],
                        [
                            "eid1803",
                            "display",
                            750,
                            0,
                            "linear",
                            "${d_4}",
                            'none',
                            'block'
                        ],
                        [
                            "eid1206",
                            "display",
                            0,
                            0,
                            "linear",
                            "${d_5}",
                            'none',
                            'none'
                        ],
                        [
                            "eid1211",
                            "display",
                            500,
                            0,
                            "linear",
                            "${d_5}",
                            'none',
                            'block'
                        ],
                        [
                            "eid1797",
                            "display",
                            630,
                            0,
                            "linear",
                            "${d_5}",
                            'block',
                            'none'
                        ],
                        [
                            "eid1798",
                            "display",
                            1130,
                            0,
                            "linear",
                            "${d_5}",
                            'none',
                            'block'
                        ],
                        [
                            "eid1203",
                            "display",
                            0,
                            0,
                            "linear",
                            "${d_2}",
                            'block',
                            'block'
                        ],
                        [
                            "eid1805",
                            "display",
                            630,
                            0,
                            "linear",
                            "${d_2}",
                            'block',
                            'block'
                        ],
                        [
                            "eid2031",
                            "display",
                            0,
                            0,
                            "linear",
                            "${d_1}",
                            'none',
                            'none'
                        ],
                        [
                            "eid2032",
                            "display",
                            1250,
                            0,
                            "linear",
                            "${d_1}",
                            'none',
                            'block'
                        ],
                        [
                            "eid1204",
                            "display",
                            0,
                            0,
                            "linear",
                            "${d_3}",
                            'none',
                            'none'
                        ],
                        [
                            "eid1210",
                            "display",
                            365,
                            0,
                            "linear",
                            "${d_3}",
                            'none',
                            'block'
                        ],
                        [
                            "eid1800",
                            "display",
                            630,
                            0,
                            "linear",
                            "${d_3}",
                            'block',
                            'none'
                        ],
                        [
                            "eid1799",
                            "display",
                            995,
                            0,
                            "linear",
                            "${d_3}",
                            'none',
                            'block'
                        ]
                    ]
                }
            }
        };

    AdobeEdge.registerCompositionDefn(compId, symbols, fonts, scripts, resources, opts);

    if (!window.edge_authoring_mode) AdobeEdge.getComposition(compId).load("laberinto_edgeActions.js");
})("EDGE-4679551");
