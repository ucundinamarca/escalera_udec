
var ST = "#Stage_";
var S = "Stage_";
ivo.info({
    title: "Escalera",
    autorotation: "Edilson Laverde Molina",
    date:  "10/03/2021",
    email: "edilsonlaverde_182@hotmail.com",
    icon: 'https://www.ucundinamarca.edu.co/images/iconos/favicon_verde_1.png',
    facebook: "https://www.facebook.com/edilson.laverde.molina"
});
var audios=[
    {url:"sonidos/click.mp3",      name:"clic"     },
    {url:"sonidos/good.mp3",       name:"good"     },
    {url:"sonidos/fail.mp3",       name:"fail"     },
    {url:"sonidos/coin.mp3",       name:"chicharra"}
];

index=0;
var reloj=null;
var contador=0;
var pasos=[
    {x:"0",	    y:"0",      rotation:1, salto:0 ,p:0}, //0
    {x:"90",    y:"0",      rotation:1, salto:0 ,p:0}, //1
    {x:"180",   y:"0",      rotation:1, salto:0 ,p:0}, //2
    {x:"270",   y:"0",      rotation:1, salto:0 ,p:0}, //3
    {x:"360",   y:"0",      rotation:1, salto:0 ,p:0}, //4
    {x:"440",   y:"0",      rotation:1, salto:0 ,p:0}, //5
    {x:"530",   y:"0",      rotation:1, salto:0 ,p:0}, //6
    {x:"620",   y:"0",      rotation:1, salto:12,p:1}, //7
    {x:"700",   y:"0",      rotation:1, salto:0 ,p:0}, //8
    {x:"700",   y:"-75",    rotation:-1,salto:0 ,p:0}, //9
    {x:"700",   y:"-150",   rotation:-1,salto:0 ,p:0}, //10
    {x:"620",   y:"-150",   rotation:-1,salto:0 ,p:0}, //11
    {x:"530",   y:"-150",   rotation:-1,salto:7 ,p:0}, //12
    {x:"440",   y:"-150",   rotation:-1,salto:0 ,p:0}, //13
    {x:"360",   y:"-150",   rotation:-1,salto:0 ,p:0}, //14
    {x:"270",   y:"-150",   rotation:-1,salto:0 ,p:0}, //15
    {x:"180",   y:"-150",   rotation:-1,salto:0 ,p:0}, //16
    {x:"90",	y:"-150",	rotation:-1,salto:26,p:1}, //17
    {x:"0",	    y:"-150",	rotation:-1,salto:0 ,p:0}, //18
    {x:"-85",	y:"-150",	rotation:-1,salto:0 ,p:0}, //19
    {x:"-170",	y:"-150",	rotation:-1,salto:0 ,p:0}, //20
    {x:"-170",	y:"-225",	rotation:1, salto:0 ,p:0},  //21
    {x:"-170",	y:"-300",	rotation:1, salto:0 ,p:0},  //22
    {x:"-85",   y:"-300",	rotation:1, salto:0 ,p:0},  //23
    {x:"0",  	y:"-300",	rotation:1, salto:0 ,p:0},  //24
    {x:"90", 	y:"-300",	rotation:1, salto:0 ,p:0},  //25
    {x:"180",   y:"-300",	rotation:1, salto:17,p:0},  //26
    {x:"270",  	y:"-300",	rotation:1, salto:0 ,p:0},  //27
    {x:"360",  	y:"-300",	rotation:1, salto:0 ,p:0},  //28
    {x:"440",  	y:"-300",	rotation:1, salto:0 ,p:0},  //29
    {x:"530",  	y:"-300",	rotation:1, salto:0 ,p:0},  //30
    {x:"620",  	y:"-300",	rotation:1, salto:0 ,p:0},  //31
    {x:"700",  	y:"-300",	rotation:1, salto:0 ,p:0},  //32
    {x:"700",	y:"-375",	rotation:-1,salto:0 ,p:0}, //33
    {x:"700",	y:"-450",	rotation:-1,salto:0 ,p:0}, //34
    {x:"620",	y:"-450",	rotation:-1,salto:0 ,p:0}, //35
    {x:"530",	y:"-450",	rotation:-1,salto:0 ,p:0}, //36
    {x:"440",	y:"-450",	rotation:-1,salto:0 ,p:0}, //37
    {x:"360",	y:"-450",	rotation:-1,salto:0 ,p:0}, //38
    {x:"270",	y:"-450",	rotation:-1,salto:0 ,p:0}, //39
    {x:"160",	y:"-450",	rotation:-1,salto:0 ,p:0},  //40
    {x:"100",	y:"-450",	rotation:-1,salto:0 ,p:0}  //fin
];
var preguntas=[
    {
        q:"1) ¿A qué valor corresponde la siguiente definición?  “Actúo siempre con fundamento en la verdad, cumpliendo mis deberes con transparencia y rectitud, y siempre favoreciendo el interés general”?",
        r:[
            "a) Honestidad",
            "b) Compromiso ",
            "c) Diligencia ",
            "d) Respeto ",
            "e) Justicia"
        ],
        c:0
    },
    {
        q:"2) ¿Cuántos valores tiene función pública para los servidores públicos?",
        r:[
            "a) 10",
            "b) 3",
            "c) 5",
            "d) 4",
        ],
        c:2
    },
    {
        q:"3) ¿A qué valor corresponde la siguiente definición “¿Soy consistente de la importancia de mi rol como servidor público y estoy en disposición permanente para comprender y resolver las necesidades de las personas con las que me relaciono en mis labores cotidianas, buscando siempre mejorar su bienestar”?",
        r:[
            "a)Honestidad",
            "b)Compromiso",
            "c)Diligencia",
            "d)Respeto",
            "e)Justicia"
        ],
        c:1
    },
    {
        q:"4) ¿A qué valor corresponde la siguiente definición “Cumplo con los deberes, funciones y responsabilidades asignadas a mi cargo de la mejor manera posible, con atención, prontitud, destreza y eficiencia, para así optimizar el uso de los recursos del Estado”?",
        r:[
            "a)Honestidad",
            "b)Compromiso",
            "c)Diligencia",
            "d)Respeto",
            "e)Justicia"
        ],
        c:2
    },
    {
        q:"5)¿A qué valor corresponde la siguiente definición “Actuó con imparcialidad garantizando los derechos de las personas, con equidad, igualdad y sin discriminación”?",
        r:[
            "a)Honestidad",
            "b)Compromiso",
            "c)Diligencia",
            "d)Respeto",
            "e)Justicia"
        ],
        c:4
    },
    {
        q:"6) Usted cometió un error al enviar el presupuesto anual con un “0” de menos. Si confiesa el error es posible que pierda el puesto. Usted en este caso:",
        r:[
            "a) Habla con su jefe inmediato para resolver la situación",
            "b) Se hace el loco"
        ],
        c:0
    },
    {
        q:"7) Todos los empleados de una entidad deben marcar ficha a las 5:30 p.m. Su amigo sale temprano por una situación personal y le pide el favor de firmar la ficha por él. Usted:",
        r:[
            "a) Le hace el favor a su amigo",
            "b) No le hace el favor a su amigo"
        ],
        c:1
    },
    {
        q:"8) Usted es la última persona que recibe un informe para revisar y enviar al jefe. Al recibirlo, se da cuenta que tiene errores de ortografía. Usted:",
        r:[
            "a) Devuelve el informe",
            "b) Corrige los errores"
        ],
        c:0
    },
    {
        q:"9) Estas a un paso de subir tu escalera de la integridad, responde la pregunta y podrás avanzar. ¿A qué valor corresponde la siguiente definición “Reconozco, valoro y trato de manera digna a todas las personas, con sus virtudes y defectos, sin importar su labor, su procedencia, títulos o cualquier otra condición”? aparecen las siguientes opciones",
        r:[
            "a) Honestidad",
            "b) Compromiso",
            "c) Diligencia",
            "d) Respeto",
            "e) Justicia"
        ],
        c:3
    },
    {
        q:"10) “Estoy abierto al diálogo y a la comprensión a pesar de las perspectivas y opiniones distintas a las mías. No hay nada que no se pueda solucionar hablando y escuchando al otro” es uno de los principios de acción del valor JUSTICIA",
        r:[
            "a) Honestidad",
            "b) Compromiso",
            "c) Diligencia",
            "d) Respeto",
            "e) Justicia"
        ],
        c:3
    },
    {
        q:"11) “Estoy abierto al diálogo y a la comprensión a pesar de las perspectivas y opiniones distintas a las mías. No hay nada que no se pueda solucionar hablando y escuchando al otro” es uno de los principios de acción del valor JUSTICIA",
        r:[
            "a) Verdadero",
            "b) Falso"
        ],
        c:1
    },
    {
        q:"12) “Facilito el acceso a la información pública completa, veraz, oportuna y comprensible a través de los medios destinados para ello” es uno de los principios de acción del valor HONESTIDAD.",
        r:[
            "a) Verdadero",
            "b) Falso"
        ],
        c:0
    },
    {
        q:"13) “Siempre estoy dispuesto a ponerme en los zapatos de las personas. Entender su contexto, necesidades y requerimientos es el fundamento de mi servicio y labor” es uno de los principios de acción del valor COMPROMISO.",
        r:[
            "a) Verdadero",
            "b) Falso"
        ],
        c:0
    },
    {
        q:"14) “Tomo decisiones estableciendo mecanismos de diálogo y concertación con todas las partes involucradas” es uno de los principios de acción del valor RESPETO.",
        r:[
            "a) Verdadero",
            "b) Falso"
        ],
        c:1
    },
    {
        q:"15) “Cumplo con los tiempos estipulados para el logro de cada obligación laboral. A fin de cuentas, el tiempo de todos es oro” es uno de los principios de acción del valor DILIGENCIA",
        r:[
            "a) Verdadero",
            "b) Falso"
        ],
        c:0
    },
    {
        q:"16) El Código General de Integridad define las conductas ideales dentro del marco de la legalidad.",
        r:[
            "a) Verdadero",
            "b) Falso"
        ],
        c:0
    },
    {
        q:"17) Cada entidad tendrá su propio Código de Integridad.",
        r:[
            "a) Verdadero",
            "b) Falso"
        ],
        c:0
    },
    {
        q:"18) EL Código General de Integridad define qué es la corrupción y cómo atacarla.",
        r:[
            "a) Verdadero",
            "b) Falso"
        ],
        c:1
    },
    {
        q:"19) En la universidad de Cundinamarca se creó el código de integridad, respondiendo a la autonomía universitaria y que los funcionarios en su rol de libertad se autocontrolan, se autorregulan y se autoadministran.",
        r:[
            "a) Verdadero",
            "b) Falso"
        ],
        c:0
    },
    {
        q:"20) El Código General de Integridad del Servicio Público Colombiano fue creado por Liliana Caballero",
        r:[
            "a) Verdadero",
            "b) Falso"
        ],
        c:1
    },
    {
        q:"21) El Código General de Integridad reemplazará todos los códigos de integridad de las entidades públicas de la Rama Ejecutiva y se establecerá como un único código del servicio público colombiano.",
        r:[
            "a) Verdadero",
            "b) Falso"
        ],
        c:0
    },
    {
        q:"22) En su casa se acabó el papel higiénico y usted no tiene tiempo para comprar otro. Decide coger el papel del baño de la oficina y a la salida del edificio el portero descubre el rollo de papel.",
        r:[
            "a) Se inventa que es suyo y dice que a usted le gusta llevar su propio papel higiénico.",
            "b) Dice que es para un proyecto ambiental",
            "c) Se hace el loco",
            "d) Pide perdón y entrega el papel"
        ],
        c:3
    },
    {
        q:"23) La dificultad de establecer valores éticos radica en la necesidad de atender intereses de las distintas partes que pueden ser contrapuestos",
        r:[
            "a) Verdadero",
            "b) Falso"
        ],
        c:1
    },
    {
        q:"24) La tarea fundamental de los valores y la integridad es lograr equilibrio entre los intereses de la dirección, los de la empresa, sus empleados, los proveedores, clientes, competidores y el público",
        r:[
            "a) Verdadero",
            "b) Falso"
        ],
        c:0
    },
    {
        q:"25) Una de las siguientes palabras no es un valor:",
        r:[
            "a) Honestidad",
            "b) Como voy yo",
            "c) Respeto",
            "d) Diligencia",
            "e) Compromiso"
        ],
        c:1
    },
    {
        q:"26) El control debe basarse en la integridad y el compromiso ético de las directivas y administrativos",
        r:[
            "a) Verdadero",
            "b) Falso"
        ],
        c:0
    },
    {
        q:"27) Mediante el proceso de selección de personal las organizaciones tratan de objetivar cual es el candidato más adecuado para cada puesto de trabajo o bien, quien es el mejor candidato para desempeñar de forma más eficaz los requerimientos y exigencias demandadas.",
        r:[
            "a) Verdadero",
            "b) Falso"
        ],
        c:0
    },
    {
        q:"28) Una de las siguientes acciones hace parte del ciclo del servidor público:",
        r:[
            "a) Correspondencia",
            "b) Reclutamiento",
            "c) Despido",
            "d) Amonestaciones"
        ],
        c:1
    },
    {
        q:"29) La definición “una persona, recta, proba e intachable” hace referencia al concepto de:",
        r:[
            "a) Compromiso",
            "b) honestidad",
            "c) integridad",
            "d) Resiliente"
        ],
        c:2
    },
    {
        q:"30) El reclutamiento, la inducción, el desarrollo y el retiro son conceptos que pertenecen a:",
        r:[
            "a) Verdadero",
            "b) Falso"
        ],
        c:0
    },
    {
        q:"31) El Código Autonómico de la Universidad de Cundinamarca Generación SIGLO21 está compuesto de dos secciones el código de buen gobierno, que se concibe como un mecanismo importante dentro de la gestión institucional, y de integridad, en el cual se establece los valores y principios que caracterizan el accionar de los servidores públicos.",
        r:[
            "a) código de integridad",
            "b) ciclo de vida del servidor público",
            "c) fomento y desarrollo",
            "d) Planeación estratégica."
        ],
        c:1
    },
    {
        q:"32) La definición de Barrera (2019) “Una persona que va más allá de sí misma, que cada día mejora y se transforma, dejando de lado los interés propios, tomando la posición del otro, los seres vivos y la naturaleza, que se perfecciona así misma antes y no necesariamente a través de la tecnología y la ciencia; si no que además lucha por su felicidad, realiza su plan de vida, sin desvanecer a los demás y dando lo mejor como profesional, emprendedor innovador y transformador de su entorno; que se explica y piensa en el dialogo con la comunidad y la convivencia” hace referencia al concepto de:",
        r:[
            "a) Translocal",
            "b) Planeación",
            "c) Integridad",
            "d) Transhumanidad",
            "e) Humanidad"
        ],
        c:3
    },
    {
        q:"33) ¿Cuáles de las siguientes entidades públicas o dependencias de la Rama Ejecutiva son las encargadas de promover la transparencia, la integridad pública o la lucha contra la corrupción?",
        r:[
            "a) Procuraduría y Contraloría",
            "b) Departamento Administrativo de la Función Pública, Secretaría de Transparencia de la Presidencia de la República y Agencia Colombiana de Contratación Pública Colombia Compra Eficiente",
            "c) Comisión Nacional de Moralización y las Comisiones Regionales de Moralización",
            "d) Comisión Nacional Ciudadana para la Lucha contra la Corrupción y Veedurías",
        ],
        c:1
    },
    {
        q:"34) ¿Cuál de las siguientes opciones NO hace parte del marco normativo para la promoción de la transparencia, la integridad pública y la lucha contra la corrupción en Colombia?",
        r:[
            "a) Política de Participación Ciudadana",
            "b) Estatuto Anticorrupción",
            "c) Ley de Transparencia y Acceso a la Información",
            "d) Código de Policía"
        ],
        c:3
    },
    {
        q:"35) ¿Cuál de las siguientes opciones NO hace parte del marco normativo para la promoción de la transparencia, la integridad pública y la lucha contra la corrupción en Colombia?",
        r:[
            "a) Política de Participación Ciudadana",
            "b) Estatuto Anticorrupción",
            "c) Ley de Transparencia y Acceso a la Información",
            "d) Código de Policía"
        ],
        c:3
    },
    {
        q:"36) ¿Qué instancia fortaleció el Estatuto Anticorrupción, Ley 1474 de 2011, integrada por las cabezas de los órganos de control para coordinar las acciones conjuntas, así como el intercambio de información para la lucha contra la corrupción?",
        r:[
            "a) Veedurías Ciudadanas",
            "b) Comisión Nacional Ciudadana de Lucha contra la Corrupción",
            "c) Comisión Nacional y Regionales de Moralización",
            "d) Departamento Administrativo de la Función Pública"
        ],
        c:2
    },
    {
        q:"37) En el contexto general de la gestión pública, ¿qué se entiende por transparencia?",
        r:[
            "a) La cualidad de un gobierno, empresa, organización o persona, de ser abierta en la divulgación de información, normas, planes, procesos y acciones.",
            "b) Cualidad de un objeto que puede verse a través de él.",
            "c) Capacidad de un gobierno para establecer un diálogo con los ciudadanos sobre la gestión y el desempeño.",
            "d) Expresión del control social relacionada con la petición de información."
        ],
        c:0
    },
    {
        q:"38) ¿Cuál de las siguientes opciones NO es uno de los propósitos principales de la 'Ley de Transparencia y del Derecho de Acceso a la Información Pública Nacional'?",
        r:[
            "a) Posicionar el acceso a la información como un derecho fundamental plenamente reglamentado.",
            "b) Ampliar el ámbito de aplicación del sistema de acceso a la información, aumentando el número de sujetos obligados, garantizando así el derecho en su expresión más amplia.",
            "c) Clarificar y ampliar los instrumentos y herramientas para el ejercicio del derecho fundamental.",
            "d) Establecer y castigar nuevas infracciones cometidas por los servidores públicos en atención al ciudadano."
        ],
        c:3
    },
    {
        q:"39) ¿Cuáles son los cinco valores que están contenidos en el Código de Integridad y que deben orientar las conductas de los servidores públicos en Colombia?",
        r:[
            "a) Honestidad, respeto, compromiso, diligencia y justicia.",
            "b) Transparencia, legalidad, ética de lo público, integridad y orgullo.",
            "c) Probidad, ética, cultura, coherencia y compromiso.",
            "d) Lealtad, trabajo en equipo, orgullo, justicia y honestidad."
        ],
        c:0
    },
    {
        q:"40) ¿Cuál de las siguientes NO es una característica del Código de Integridad-valores del servicio público?",
        r:[
            "a) Es construido participativamente y publicado con un lenguaje claro y sencillo.",
            "b) Es un código único y general para todas las entidades públicas de Colombia.",
            "c) Presenta un enfoque de cambio cultural de las conductas cotidianas y tiene una finalidad pedagógica.",
            "d) Establece castigos y multas a los servidores públicos que no tengan un comportamiento ejemplar en sus labores diarias.",
        ],
        c:3
    }


];
midado=0;
var puntos=0;
var puesto=0;
var old_puesto=0;
function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min) + min); //The maximum is exclusive and the minimum is inclusive
}
function walker(n){
    let puesto=n;
    ivo(ST+"ava").hide();
   
    
    TweenMax.to(ST+"ava",1, {
        x:pasos[puesto].x,
        y:pasos[puesto].y,
        scaleX:pasos[puesto].rotation,
        onStart:function(){
            sym2.getSymbol("avatar").play();
        },
        onComplete:function(){
            sym2.getSymbol("avatar").stop();
            ivo(ST+"ava").show();
        }
    });
    
}
function pasear(n){
    if(n==false){
        TweenMax.to(ST+"ava",1.5, {
            x:pasos[puesto].x,
            y:pasos[puesto].y,
            scaleX:pasos[puesto].rotation, onComplete:function(){
                if(parseInt(puesto)<parseInt(41)){
                    stage3.play();
                    c.stop();
                }else{
                    ivo.play("chicharra");
                    
                    Swal.fire({
                        title: '<strong>Retroalimentación</strong>',imageUrl: './images/home.png',
                        imageHeight: 100,
                        imageAlt: 'A tall image',
                        showCancelButton: false,
                        showConfirmButton: false,
                        html:
                          'Recuerde que los valores son pautas, base de la conducta tanto individual como colectiva de los individuos. ' ,
                        
                      }); 
                      ivo(".swal2-image").on("click",function(){
                        window.location.href="https://virtual.ucundinamarca.edu.co/course/view.php?id=15147&section=1";
                      }).css("cursor","pointer");
                    Game_UdeC.data.quiz=preguntas;
                    Game_UdeC.data.game="Escalera";
                    Game_UdeC.data.url_game ='https://virtual.ucundinamarca.edu.co/red/Otros/games/escalera_udec/laberinto.html';
                    Game_UdeC.save();
                }
                sym2.getSymbol("avatar").stop();
            }
        });
    }else{
        old_puesto+=1;
        pasear2(old_puesto);
    }

}
function pasear2(){
    if(old_puesto!=puesto){
        TweenMax.to(ST+"ava",1, {
            x:pasos[old_puesto].x,
            y:pasos[old_puesto].y,
            scaleX:pasos[old_puesto].rotation, onComplete:function(){
                old_puesto+=1;
                pasear2(old_puesto);
            }
        });
    }else{
        pasear(false);
    }
}
var sym2=null;
function main(sym) {
    dado=true;
    sym2=sym;
    udec = ivo.structure({
            created:function(){
            t=this;
            ivo(ST+"m").text("0");
                //precarga audios//
            ivo.load_audio(audios,onComplete=function(){
                ivo(ST+"preload").hide();
                ivo(".dados").hide();
                ivo(".ins").hide();
                ivo(ST+"page_1").show();
                ivo(ST+"capa").hide();
                t.events();
                t.animation();
                intro.play();
                Game_UdeC.info_moodle();
            });
            },
            methods: {
                launch:function(n){
                    try {
                        ivo(ST+"Text2").html(`
                            <div>
                                <p>${preguntas[n].q}</p>
                                <ul id="q"></ul>
                            </div>
                        `);
                        let p=0;
                        for(l of preguntas[n].r){
                            if(p==preguntas[n].c){
                                ivo("#q").append(`<li data-correct="true" >${l}</li>`);
                            }else{
                                ivo("#q").append(`<li data-correct="false" >${l}</li>`);
                            }
                            p+=1;
                        }
                        ivo("#q li").on("click",function(){
                            let data = ivo(this).attr("data-correct");
                            preguntas[puesto-1].select= ivo(this).text();
                            if(data=="true"){
                                preguntas[puesto-1].correct="si";
                                ivo.play("good");
                                console.log(pasos[puesto].salto);
                                if(pasos[puesto].salto!=0){
                                    if(pasos[puesto].p==1){
                                        old_puesto=puesto;
                                        puesto=pasos[puesto].salto;
                                        pasear(false);
                                        t.launch(puesto-1);
                                    }else{
                                        setTimeout(function(){
                                            stage2.play().timeScale(1);
                                        },1500);
                                    }
                                }else{
                                    setTimeout(function(){
                                        stage2.play().timeScale(1);
                                    },1500);
                                }
                            }else{
                                //si se devuelve
                                preguntas[puesto-1].correct="no";
                                if(pasos[puesto].salto!=0){
                                    if(pasos[puesto].p==0){
                                        old_puesto=puesto;
                                        puesto=pasos[puesto].salto;
                                        pasear(false);
                                        t.launch(puesto-1);
                                    }else{
                                        if(midado==0){
                                            var retorno=0;
                                        }else{
                                            var retorno= puesto-midado;
                                        }
                                        old_puesto=retorno;
                                        puesto=retorno;
                                        walker(retorno);
                                        
                                        setTimeout(function(){
                                            stage2.play().timeScale(1);
                                        },1500);
                                    }
                                }else{
                                    if(midado==0){
                                        var retorno=0;
                                    }else{
                                        var retorno= puesto-midado;
                                    }
                                    old_puesto=retorno;
                                    puesto=retorno;
                                    walker(retorno);
                                    
                                    setTimeout(function(){
                                        stage2.play().timeScale(1);
                                    },1500);
                                }
                                ivo.play("fail");

                            }
                            ivo(ST+"n1").text(puesto);
                            stage3.reverse();
                        });
                    } catch (error) {
                    }
                },
                events:function(){
                    var t=this;
                    ivo(ST+"ok").on("click",function(){
                        stage1.reverse().timeScale(3);
                        stage2.play().timeScale(1);
                        ivo.play("clic");
                    });

                    ivo(ST+"icon1").on("click",function(){
                        ivo(".ins").hide();
                        ivo(ST+"page_1").show();
                        ivo.play("clic");
                    });

                    $(ST+"icon2").addClass("animated infinite tada");
                    ivo(ST+"icon2").on("click",function(){
                        $(ST+"icon2").removeClass("animated infinite tada");
                        ivo(".ins").hide();
                        ivo(ST+"page_2").show();
                        ivo.play("clic");
                    });

                    ivo(ST+"dado, .dados").on("click",function(){
                        if(dado){
                            dado=false;
                            ivo(".dados").hide();
                            sym2.getSymbol("dado").play();
                            ivo.play("clic");
                            midado=getRandomInt(1,6);
                            old_puesto=puesto;
                            puesto=puesto+midado;
                            
                            if(puesto>40){
                                puesto=41;
                                
                            }
                            ivo(ST+"capa").show();
                            setTimeout(function(){
                                ivo(ST+"d_"+midado).show();
                                ivo(ST+"n1").text(puesto);
                            },1300);
                        }
                    });
                    ivo(ST+"btn_seg").on("click",function(){
                        if(dado==false){
                            stage2.reverse().timeScale(10);
                            ivo.play("clic");
                            sym.getSymbol("avatar").play(0);
                            pasear(true);
                            ivo(ST+"n1").text(puesto);
                            t.launch(puesto-1);
                            ivo(ST+"capa").hide();
                            dado=true;
                        }
                    });
                    ivo(ST+"btn_seg2").on("click",function(){
                        intro.reverse().timeScale(2);
                        c = new Cronometro({
                            min:10,
                            id:"Stage_time",
                            finish:function(){
                               
                            }
                        });
                        ivo.play("clic");
                        stage2.play();
                    });
                },
                animation:function(){
                    intro = new TimelineMax();
                    intro.append(TweenMax.from(ST+"stage_instruccion", .8,          {x:1300,opacity:0}), 0);
                    intro.append(TweenMax.from(ST+"contenido", .8,             {x:1300,opacity:0}), 0);
                    intro.stop();

                  
                    stage2 = new TimelineMax();
                    stage2.append(TweenMax.from(ST+"stage_dado", .8,      {x:1300,opacity:0}), 0);
                    stage2.append(TweenMax.from(ST+"s_dado", .8,      {x:1300,opacity:0}), 0);
                    stage2.stop();

                    stage3 = new TimelineMax();
                    stage3.append(TweenMax.fromTo(ST+"stage_question", .4,{opacity:0,y:900},{opacity:1,y:0}), 0);
                    stage3.append(TweenMax.from(ST+"s_q", .8,      {x:1300,opacity:0}), 0);
                    stage3.stop();

                }
            }
    });
}
