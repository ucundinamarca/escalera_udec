class Cronometro {
    constructor(C){
        this.$min=    C.min;
        this.$id=     C.id;
        this.$seg=    0;
        this.$finish= C.finish;
        this.play();
    }
    get_Fortmat(text){
        let texto=text.toString().length==1 ? "0"+text : text;
        return texto;
    }
    play(){
        this.interval = setInterval(()=> {
            this.render();
        },1000);
    }
    stop(){
        clearInterval(this.interval)
    }
    render(){
        this.counter();
        document.getElementById(this.$id).textContent = this.get_Fortmat( this.$min )+":" + this.get_Fortmat(this.$seg);
    }
    counter(){
        if(this.$seg==0 && this.$min==0){
            this.stop();
            this.$finish();
        }else{
            if(this.$seg==0){
                this.$seg = 59;
                this.$min-=1;
            }else{
                this.$seg -= 1;
            }
        }
    }
}