/***********************
* Acciones de composición de Adobe Edge Animate
*
* Editar este archivo con precaución, teniendo cuidado de conservar 
* las firmas de función y los comentarios que comienzan con "Edge" para mantener la 
* capacidad de interactuar con estas acciones en Adobe Edge Animate
*
***********************/
(function($, Edge, compId){
var Composition = Edge.Composition, Symbol = Edge.Symbol; // los alias más comunes para las clases de Edge

   //Edge symbol: 'stage'
   (function(symbolName) {
      
      
      Symbol.bindElementAction(compId, symbolName, "document", "compositionReady", function(sym, e) {
                  yepnope({
                  	nope:[
                  		'css/udec.css',
                        'js/cronometro.js',
                        'js/sweetalert.min.js',
                        'js/jquery-3.3.1.min.js',
                        'js/jquery-ui.min.js',
                        'js/ivo.js',
                        'js/TweenMax.min.js',
                        'js/howler.min.js',
                        'js/game.js?v=0.0.1',
                        'js/main.js'
                  	],
                     complete: init
                  }); // end of yepnope
                  function init() {
                  	main(sym);
                  }

      });
      //Edge binding end

   })("stage");
   //Edge symbol end:'stage'

   //=========================================================
   
   //Edge symbol: 'avatar'
   (function(symbolName) {   
   
      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 500, function(sym, e) {
         sym.play(0);

      });
      //Edge binding end

   })("avatar");
   //Edge symbol end:'avatar'

   //=========================================================
   
   //Edge symbol: 'dado'
   (function(symbolName) {   
   
   })("dado");
   //Edge symbol end:'dado'

})(window.jQuery || AdobeEdge.$, AdobeEdge, "EDGE-4679551");